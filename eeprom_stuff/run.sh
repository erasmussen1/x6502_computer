
#
# head -c 256k < /dev/urandom | srec_cat - -bin -o -

## Write file to eeprom
#head -c 32k < /dev/urandom | srec_cat - -bin -o test01.srec
#sudo minipro -p "AT28C256" -f srec -w test01.srec

## Read eeprom to file
#sudo minipro -p "AT28C256" -f srec -r test02.srec

## Erase eeprom and read eeprom to file
#sudo minipro -p "AT28C256" -f srec -E
#sudo minipro -p "AT28C256" -f srec -r test03.srec


## Write file to eeprom
# head -c 32k < /dev/urandom | srec_cat - -bin -Address_Length=2 -obs=16 -o test04.srec
# head -c 32k < /dev/zero > mytmp

# cat mytmp | srec_cat - -bin -Address_Length=2 -obs=16 -o test04.srec
# sudo minipro -p "AT28C256" -f srec -w test04.srec

./mkrom.py
cat rom.bin | srec_cat - -bin -Address_Length=2 -obs=16 -o test05.srec
sudo minipro -p "AT28C256" -f srec -w test05.srec

